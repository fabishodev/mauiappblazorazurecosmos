﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiAppBlazorAzureCosmos.Models
{

    using Newtonsoft.Json;
    public class TeamFootball
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey { get; set; }
        public string Name { get; set; }
        public string Foundation { get; set; }
        public List<Players> Players { get; set; }
    }

    public class Players
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public Position Position { get; set; }
        public Boolean Enabled { get; set; }
    }

    public enum Position : int
    {
        NONE = 0,
        GOALKEEPER = 1,
        FORWARd = 2,
        DEFENCE = 3,
        MIDFIELDER = 4,
        COACH = 5,
    }

}
