﻿using MauiAppBlazorAzureCosmos.Models;
using MauiAppBlazorAzureCosmos.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiAppBlazorAzureCosmos.Data
{
    public class TeamsService
    {

        public async Task<List<TeamFootball>> GetTeamsAsync()
        {
            try
            {
                List<MauiAppBlazorAzureCosmos.Models.TeamFootball> teams;
                var cosmostSvc = new MauiAppBlazorAzureCosmos.Services.DBCosmosAzure();
                await cosmostSvc.CreateDatabase();
                await cosmostSvc.CreateDocumentCollection();
                teams = await cosmostSvc.GetAsync();
                return teams;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> SaveTeamAsync(TeamFootball team, bool isNew)
        {
            try
            {
                var cosmostSvc = new MauiAppBlazorAzureCosmos.Services.DBCosmosAzure();
                await cosmostSvc.CreateDatabase();
                await cosmostSvc.CreateDocumentCollection();
                await cosmostSvc.SaveAsync(team, isNew);
                return true;
            }
            catch (Exception ex)
            {               
                return false;
            }            
        }

        public async Task<bool> DeleteTeamAsync(TeamFootball team)
        {
            try
            {
                string id = team.Id;
                string partitionKey = "Mexico";
                var cosmostSvc = new MauiAppBlazorAzureCosmos.Services.DBCosmosAzure();
                await cosmostSvc.CreateDatabase();
                await cosmostSvc.CreateDocumentCollection();
                await cosmostSvc.DeleteAsync(id, partitionKey);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

    }
}
