﻿using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiAppBlazorAzureCosmos
{
    public static class Constants
    {
        public static readonly string EndPointUri = "";
        public static readonly string PrimaryKey = "";
        public static CosmosClient cosmosClient;
        public static string databaseId = "MyDBMTWDM2022";
        public static string containerId = "Teams";
        public static Database database;
        public static Container container;
    }
}
