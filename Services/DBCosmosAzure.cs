﻿using MauiAppBlazorAzureCosmos.Models;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiAppBlazorAzureCosmos.Services
{
    public class DBCosmosAzure : IDBCosmosAzure
    {
        public DBCosmosAzure()
        {
            Constants.cosmosClient = new Microsoft.Azure.Cosmos.CosmosClient(Constants.EndPointUri, Constants.PrimaryKey);
        }

        public async Task CreateDatabase()
        {
            try
            {
                Constants.database = await Constants.cosmosClient.CreateDatabaseIfNotExistsAsync(Constants.databaseId);
            }
            catch (CosmosException ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task CreateDocumentCollection()
        {
            try
            {
                Constants.container = await Constants.database.CreateContainerIfNotExistsAsync(Constants.containerId, "/partitionKey");
            }
            catch (CosmosException ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<List<TeamFootball>> GetAsync()
        {
            List<TeamFootball> teams = new List<TeamFootball>();
            try
            {
                var sqlQuery = "SELECT * FROM c WHERE c.partitionKey = 'Mexico'";
                QueryDefinition queryDefinition = new QueryDefinition(sqlQuery);
                FeedIterator<TeamFootball> queryResultSetIterator = Constants.container.GetItemQueryIterator<TeamFootball>(queryDefinition);
                while (queryResultSetIterator.HasMoreResults)
                {
                    FeedResponse<TeamFootball> currentResultSet = await queryResultSetIterator.ReadNextAsync();

                    foreach (var item in currentResultSet)
                    {
                        teams.Add(item);
                    }
                }
            }
            catch (CosmosException ex)
            {

                throw new Exception(ex.Message);
            }

            return teams;
        }

        public async Task SaveAsync(TeamFootball teamFootball, bool isNew)
        {
            try
            {
                if (isNew)
                {
                    ItemResponse<TeamFootball> itemResponse = await Constants.container.CreateItemAsync<TeamFootball>(teamFootball, new PartitionKey(teamFootball.PartitionKey));
                }
                else
                {
                    ItemResponse<TeamFootball> teamResponse = await Constants.container.ReadItemAsync<TeamFootball>(teamFootball.Id, new PartitionKey(teamFootball.PartitionKey));
                    var item = teamResponse.Resource;
                    teamResponse = await Constants.container.ReplaceItemAsync<TeamFootball>(teamFootball, teamFootball.Id, new PartitionKey(teamFootball.PartitionKey));
                }
            }
            catch (CosmosException ex)
            {

                throw new Exception(ex.Message);
            }

        }

        public async Task DeleteAsync(string id, string partitionKey)
        {
            try
            {
                ItemResponse<TeamFootball> teamResponse = await Constants.container.DeleteItemAsync<TeamFootball>(id, new PartitionKey(partitionKey));
            }
            catch (CosmosException ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
