﻿using MauiAppBlazorAzureCosmos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MauiAppBlazorAzureCosmos.Services
{
    public interface IDBCosmosAzure
    {
        Task CreateDatabase();
        Task CreateDocumentCollection();
        Task<List<TeamFootball>> GetAsync();
        Task SaveAsync(TeamFootball teamFootball, bool isNew);
        Task DeleteAsync(string id, string partitionKey);
    }
}
